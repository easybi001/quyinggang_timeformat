/**
 * @Title TimeFormat3.java
 * @Package com.qiancheng
 * @author quyinggang
 * @date 2016年7月11日 下午3:20:09
 * @version v2.0
 */
package com.qiancheng;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 
 * @ClassName TimeFormat3
 * @Description 时间处理相关工具类
 */
public class TimeFormat3 {
	
	//统一模式
	public static final String PATTERN = "yyyyMMddHHmmss";
	public static final String CHANGE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	//定义SimpleDateFormat对象
	public static DateFormat firstDateFormat = new SimpleDateFormat(PATTERN);
	public static DateFormat secondDateFormat = new SimpleDateFormat(CHANGE_PATTERN);
	//定义Timestamp对象
	public static Timestamp timestamp = new Timestamp(System.currentTimeMillis()); 
	
	/**
	 * @Title getTimestamp
	 * @Description 字符串->Timestamp
	 * @param source
	 * @return Timestamp
	 * @throws
	 */
	public static Timestamp stringToTimestamp(String source) {
		
		source = getChangePattern(source);
		timestamp = timestamp.valueOf(source);
		return timestamp;
	}
	
	/**
	 * @Title getString
	 * @Description Timestamp->String
	 * @param source
	 * @return String
	 * @throws
	 * day 1 week 2 month 3 year 4 second 5 
	 * minute 6 hour 7
	 */
	public static String timestampToString(Timestamp source, int type) {
		
		int week = getWeek(source);
		String stringPattern = source.toString();
		String aimString = stringPattern.split("\\.")[0];
		String stringResult = aimString.replaceAll("[^0-9]", "");
		String result = getFixedFormat(stringResult,type);
		if(type == 2) {
			result = result + week + "";
		}
		return result;
	}
	
	/**
	 * 
	 * @Title timestampOperating
	 * @Description 时间格式的运算
	 * @param source
	 * @return Timestamp
	 * @throws
	 */
	/*public static Timestamp timestampOperating(Timestamp source, long time) {
		
		long result=source.getTime();
		result+=time;
		source.setTime(result);
		return source;
	}*/
	
	/**
	 * 
	 * @Title getWeek
	 * @Description 获取具体时间在全年的第几周
	 * @param source
	 * @return int
	 * @throws
	 */
	public static int getWeek(Timestamp source) {
		
		Calendar calendar = getCalendar();
		calendar.setTime(source);
		int result = calendar.get(Calendar.WEEK_OF_YEAR);
		return result;
	}
	
	/**
	 * 
	 * @Title next
	 * @Description 时间的运算（获取指定时间的后多少天等）
	 * @param source
	 * @param day
	 * @return Timestamp
	 * @throws
	 */
	public static Timestamp next(Timestamp source, int day) {
		
		Calendar calendar = getCalendar();
		calendar.setTime(source);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		long changeTime = calendar.getTimeInMillis();
		source.setTime(changeTime);
		return source;
	}
	
	/**
	 * 
	 * @Title next
	 * @Description 时间的运算（获取指定时间的后多少天等）
	 * @param source
	 * @param day
	 * @return String
	 * @throws
	 */
	public static String next(String source, int day) {
		
		Date date = stringToDate(source);
		long change = date.getTime();
		Timestamp timeResult = new Timestamp(change);
		timeResult = next(timeResult, day);
		String result = timestampToString(timeResult, 5);
		return result;
	}
	
	/**
	 * 
	 * @Title previous
	 * @Description 时间的运算（获取指定时间的前多少天等）
	 * @param source
	 * @param day
	 * @return Timestamp
	 * @throws
	 */
	public static Timestamp previous(Timestamp source, int day) {
		
		source = next(source, -day);
		return source;
	}
	
	/**
	 * 
	 * @Title previous
	 * @Description 时间的运算（获取指定时间的前多少天等）
	 * @param source
	 * @param day
	 * @return String
	 * @throws
	 */
	public static String previous(String source, int day) {
		
		source = next(source, -day);
		return source;
	}
	
	/**
	 * 
	 * @Title getIntervelTimeList
	 * @Description 获取指定间隔时间的时间列表
	 * @param source
	 * @param intervel 
	 * @return 
	 * @throws
	 */
	public static List getIntervelTimeList(Timestamp source, int intervel) {
		
		List list = new ArrayList();
		for (int i = 0;i < intervel;i++) {
			source = next(source, 1);
			list.add(source);
		}
		return list;
	}
	

	/**
	 * 
	 * @Title getIntervelTimeList
	 * @Description 获取指定间隔时间的时间列表
	 * @param source
	 * @param intervel 
	 * @return 
	 * @throws
	 */
	public static List getIntervelTimeList(String source, int intervel) {
		
		List list = new ArrayList();
		for (int i = 0;i < intervel;i++) {
			source = next(source, 1);
			list.add(source);
		}
		return list;
	}
	
	/**
	 * 
	 * @Title getDays
	 * @Description 获取指定月份的最大天数
	 * @param month
	 * @return int
	 * @throws
	 */
	public static int getDays(int month) {
		
		int result = 0;
		Calendar calendar = getCalendar();
		int nowYear = calendar.get(Calendar.YEAR);
		if (month <= 12 && month >0) {
			if (month == 2) {
				if((nowYear % 4 == 0&&nowYear % 100 != 0)||nowYear % 400 == 0) {
					result = 29;
				} else {
					result = 28;
				}
			} else {
				calendar.set(nowYear, month-1, 1);
				calendar.roll(Calendar.DAY_OF_MONTH, false );
				result = calendar.get(Calendar.DAY_OF_MONTH);
			}
		}
		return result;
	}
	
	/**
	 * @Title getWeekDay
	 * @Description 根据周次获取该周周一具体时间等
	 * @param week void
	 * @throws
	 */
	public static List<String> getWeekDay(int week) {
		
		List<String> list = new ArrayList<String>();
		if (week > 0) {
			Calendar now = getCalendar();
			now.set(Calendar.WEEK_OF_YEAR, week);
			String result = firstDateFormat.format(now.getTime());;
			boolean isFirstDay = (now.getFirstDayOfWeek() == Calendar.MONTH);
			int day = isFirstDay?now.get(Calendar.DAY_OF_WEEK):now.get(Calendar.DAY_OF_WEEK)-1;
			String monday = next(result, 1-day).substring(0, 8);
			String weekday = next(result, 7-day).substring(0, 8);
			list.add(monday);
			list.add(weekday);
		}
		return list;
	}
	//获取日历对象
	public static Calendar getCalendar()
	{
		return Calendar.getInstance();
	}
	//对于字符串格式的统一的处理(调用stringToDate)
	private static String getChangePattern(String source) {
		
		String result = null;
		Date date=stringToDate(source);
		result = secondDateFormat.format(date);
		return result;
	}
	
	//字符串->Date
	private static Date stringToDate(String source) {
		Date result = null;
		int length = source.length();
		StringBuilder st = new StringBuilder(source);
		for(int i = st.length();i < 14;i++)//统一位数
		{
			st.append("0");
		}
		source = st.toString();
		try {
			result = firstDateFormat.parse(source);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;	
	}
	
	//根据输入字符串的格式选择输出格式
	private static String getFixedFormat(String source, int type) {
		
		String result = "";
		switch(type)
		{
			case 1:result = source.substring(0, 8);
			break;
			case 3:result = source.substring(0,6);
			break;
			case 2:case 4:result = source.substring(0,4);
			break;
			case 5:result = source;
			break;
			case 6:result = source.substring(0,12);
			break;
			case 7:result = source.substring(0,10);
			break;
		}
		return result;
	}
}
