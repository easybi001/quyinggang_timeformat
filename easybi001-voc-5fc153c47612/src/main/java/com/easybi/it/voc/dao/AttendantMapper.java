package com.easybi.it.voc.dao;

import com.easybi.it.voc.dto.Attendant;

public interface AttendantMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Attendant record);

    int insertSelective(Attendant record);

    Attendant selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Attendant record);

    int updateByPrimaryKey(Attendant record);
}