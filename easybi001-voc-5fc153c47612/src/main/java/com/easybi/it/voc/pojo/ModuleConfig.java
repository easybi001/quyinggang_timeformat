package com.easybi.it.voc.pojo;

import java.util.List;

import com.easybi.it.voc.dto.BusinessIndex;

public class ModuleConfig {
	private String title; // 标题
	private int type; // 类型
	private List<ModuleDimen> dateConfig; // 日期配置
	private List<BusinessIndex> indexList; // 指标列表
	private List<ModuleDimen> dimenList; // 维度列表
	private String chartType; // 图形类型
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public List<ModuleDimen> getDateConfig() {
		return dateConfig;
	}
	public void setDateConfig(List<ModuleDimen> dateConfig) {
		this.dateConfig = dateConfig;
	}
	public List<BusinessIndex> getIndexList() {
		return indexList;
	}
	public void setIndexList(List<BusinessIndex> indexList) {
		this.indexList = indexList;
	}
	public List<ModuleDimen> getDimenList() {
		return dimenList;
	}
	public void setDimenList(List<ModuleDimen> dimenList) {
		this.dimenList = dimenList;
	}
	public String getChartType() {
		return chartType;
	}
	public void setChartType(String chartType) {
		this.chartType = chartType;
	}
}