package com.easybi.it.voc.dao;

import com.easybi.it.voc.dto.IndexClass;

public interface IndexClassMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(IndexClass record);

    int insertSelective(IndexClass record);

    IndexClass selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IndexClass record);

    int updateByPrimaryKey(IndexClass record);
}