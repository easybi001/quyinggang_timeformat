package com.easybi.it.voc.api.dao;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.easybi.it.voc.util.GUIDHexGenerator;




/**
 * 
* @ClassName: AbstractBaseDAO 
* @Description: 基本的数据接口类，只包含一个jdbc的链接
* @author jack_li@cybespoke.com 
* @date 2015年6月30日 下午5:33:14 
*
 */
public class AbstractBaseDAO{
	
	@Resource
	protected JdbcTemplate jdbcTemplate;
	
	public static String generateId(){
		return GUIDHexGenerator.getInstance().generateId();
	}

}
