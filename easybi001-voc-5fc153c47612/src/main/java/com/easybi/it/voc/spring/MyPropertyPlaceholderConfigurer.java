package com.easybi.it.voc.spring;

import java.util.Properties;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.easybi.it.voc.api.conf.MyConfig;


/**
 * 
* @ClassName: RainbowPropertyPlaceholderConfigurer
* @Description: spring配置文件替换
* @author Jason.ma
* @date 2013年11月20日 下午2:01:47
*
 */
public class MyPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer{
	
	@Override
	protected String resolvePlaceholder(String placeholder, Properties props, int systemPropertiesMode) {
		return MyConfig.get(placeholder);
	}

	@Override
	protected String resolvePlaceholder(String placeholder, Properties props) {
		return MyConfig.get(placeholder);
	}

}
