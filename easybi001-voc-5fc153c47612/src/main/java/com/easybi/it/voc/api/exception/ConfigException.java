package com.easybi.it.voc.api.exception;

/** 
 * @ClassName: ConfigException 
 * @Description: 读取配置异常
 * @author jack_li@cybespoke.com 
 * @date 2015年6月30日 下午4:01:56 
 *  
 */
public class ConfigException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public ConfigException(String msg,Throwable e){
		super(msg,e);
	}

}
