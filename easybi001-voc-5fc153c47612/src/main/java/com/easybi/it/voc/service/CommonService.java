/**
 * @Title: CommonService.java
 * @Package: com.easybi.it.voc.service
 * @Author: jack@easestrategy.com
 * @Date: 2016年7月12日
 * @Version: v1.0
 */
package com.easybi.it.voc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easybi.it.voc.dao.BusinessIndexMapper;
import com.easybi.it.voc.dto.BusinessIndex;
import com.easybi.it.voc.iservice.ICommonService;

/**
 * @ClassName: CommonService
 * @Description: 通用service文件
 */

@Service
public class CommonService extends ICommonService{
	
	@Resource
	private BusinessIndexMapper mapper;
	
	/**
	 * 
	 * @Title getIndexInfoList
	 * @Description 根据id列表获取指标对象
	 * @param id
	 * @return List<BusinessIndex>
	 * @throws
	 */
	public List<BusinessIndex> getIndexInfoList(List<String> id) {
		
		int length = id.size();
		String[] changeElement = null;
		List<String> list = new ArrayList<String>();
		Map<Integer, String> recordMap = new HashMap<Integer, String>();
		for (int i = 0;i < length;i++) {
			String element = id.get(i);
			if (element.indexOf("_") > 0) {
				changeElement = element.split("_");
				recordMap.put(i, changeElement[1]);
				list.add(changeElement[0]);
			} else {
				list.add(element);
			}
		}
		List<BusinessIndex> businessList = mapper.getIndexInfoList(list);
		Set<Integer> set = recordMap.keySet();
		for (int index : set) {
			BusinessIndex changeBusIndex = businessList.get(index);
			changeBusIndex.setName(recordMap.get(index));
		}
		return businessList;
	}
	
	public Map<Integer,BusinessIndex> getIndexInfoMap(List<String> id) {
		
		Map<Integer, BusinessIndex> busIndexMap = new HashMap<Integer, BusinessIndex>();
		List<BusinessIndex> busIndexList = getIndexInfoList(id);
		int length = busIndexList.size();
		for (BusinessIndex busIndex : busIndexList) {
			int bid = busIndex.getId();
			busIndexMap.put(bid, busIndex);
		}
		return busIndexMap;
	}
}
