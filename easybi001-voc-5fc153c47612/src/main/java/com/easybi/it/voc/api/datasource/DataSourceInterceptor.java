package com.easybi.it.voc.api.datasource;

import org.aspectj.lang.JoinPoint;

public class DataSourceInterceptor {

	public void setdataSourceExternal(JoinPoint jp) {
		DatabaseContextHolder.setCustomerType("external");
	}
	
	public void setdataSourceInternal(JoinPoint jp) {
		DatabaseContextHolder.setCustomerType("internal");
	}
}