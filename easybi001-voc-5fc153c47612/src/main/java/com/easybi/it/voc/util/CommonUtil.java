/**   
 * @Title: CommonUtil.java 
 * @Package com.app.api.util 
 * @Description: TODO 
 * @author jack_li@cybespoke.com  
 * @date 2015年9月22日 下午6:22:39 
 * @version V1.0   
 */
package com.easybi.it.voc.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName: CommonUtil
 * @Description: TODO
 * @author jack_li@cybespoke.com
 * @date 2015年9月22日 下午6:22:39
 */
public class CommonUtil {
	private static Logger log = LoggerFactory.getLogger(CommonUtil.class);

	public static String StringFilter(String str) throws PatternSyntaxException {
		// 清除掉所有特殊字符
		String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		return m.replaceAll("").trim();
	}

	/**
	 * 生成4位随机数
	 */
	public static String getRandomCode() {
		Random random = new Random();
		String code = "";
		for (int i = 0; i < 4; i++) {
			code = code + Math.abs(random.nextInt()) % 10;
		}
		return code;
	}

	/**
	 * 获得String特定格式的系统时间
	 */
	public static String getCurrentDateTime(SimpleDateFormat formatter) {
		Date curDate = new Date(System.currentTimeMillis());
		String str = formatter.format(curDate);
		return str;
	}

	/**
	 * 把HashMap<String, String> 转换成string
	 */

	public static String changeHashMapToString(HashMap<String, String> hm) {
		String result = "";
		for (String key : hm.keySet()) {
			result += key + ":" + hm.get(key) + ",";
		}

		if (result.length() > 1) {
			result = result.substring(0, result.length() - 1);
		}

		return result;
	}

	/**
	 * 把string 转换成 HashMap<String, String>
	 */

	public static HashMap<String, String> changeStringToHashMap(String sm) {
		HashMap<String, String> result = new HashMap<String, String>();
		String[] smList = sm.split(",");
		for (int i = 0; i < smList.length; i++) {
			String[] mapStringList = smList[i].split(":");
			result.put(mapStringList[0], mapStringList[1]);
		}
		return result;
	}

	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}
}
