/**   
 * @Title: UserLoginInterceptor.java 
 * @Package com.app.api.filter 
 * @Description: TODO 
 * @author jack_li@cybespoke.com  
 * @date 2015年10月30日 下午7:18:33 
 * @version V1.0   
 */
package com.easybi.it.voc.filter;


import java.io.ByteArrayOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


/** 
 * @ClassName: UserLoginInterceptor 
 * @Description: TODO
 * @author jack_li@cybespoke.com 
 * @date 2015年10月30日 下午7:18:33  
 */
public class UserLoginInterceptor implements HandlerInterceptor  {
	
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
}
