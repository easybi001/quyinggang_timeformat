package com.easybi.it.voc.dto;

import java.sql.Timestamp;

public class IndexSingleData {
    private Integer id;

    private String dateId;

    private Integer dateTypeId;

    private Integer dimenTypeId;

    private Integer dimenId;

    private Integer indexId;

    private String indexValue;

    private Integer status;

    private Timestamp createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateId() {
        return dateId;
    }

    public void setDateId(String dateId) {
        this.dateId = dateId;
    }

    public Integer getDateTypeId() {
        return dateTypeId;
    }

    public void setDateTypeId(Integer dateTypeId) {
        this.dateTypeId = dateTypeId;
    }

    public Integer getDimenTypeId() {
        return dimenTypeId;
    }

    public void setDimenTypeId(Integer dimenTypeId) {
        this.dimenTypeId = dimenTypeId;
    }

    public Integer getDimenId() {
        return dimenId;
    }

    public void setDimenId(Integer dimenId) {
        this.dimenId = dimenId;
    }

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }

    public String getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(String indexValue) {
        this.indexValue = indexValue;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}