package com.easybi.it.voc.api.helper;

import org.springframework.context.support.AbstractApplicationContext;

import com.easybi.it.voc.api.iservice.IMyService;


/**
 * 
* @ClassName: ServiceHelper 
* @Description: 服务层帮助类
* @author jack_li@cybespoke.com 
* @date 2015年6月30日 下午5:04:43 
*
 */
public class ServiceHelper {

	private static AbstractApplicationContext springctx = null;

	public static IMyService getService(String serviceName) {
		return (IMyService) springctx.getBean(serviceName);
	}

	public static void setApplicationContext(AbstractApplicationContext ctx) {
		springctx = ctx;
	}

}
