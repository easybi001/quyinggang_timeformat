package com.easybi.it.voc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet Filter implementation class LoginFilter
 */
public class JspFilter implements Filter {
	
	
	@SuppressWarnings("unused")
	private static ServletContext servletContext = null;
	
	public final Logger logger = LoggerFactory.getLogger(JspFilter.class);
	
	@Override
	public void init(FilterConfig filterconfig) throws ServletException {
		servletContext = filterconfig.getServletContext();
	}

    /**
     * Default constructor. 
     */
    public JspFilter() {
        // TODO Auto-generated constructor stub 
    }
    
	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse rsp = (HttpServletResponse)response;
		rsp.sendError(HttpServletResponse.SC_NOT_FOUND);
		return;
	}

}
