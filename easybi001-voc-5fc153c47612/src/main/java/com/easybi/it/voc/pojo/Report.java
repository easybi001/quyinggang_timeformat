package com.easybi.it.voc.pojo;

import java.util.List;
import java.util.Map;

import com.easybi.it.voc.dto.BusinessIndex;
import com.easybi.it.voc.dto.DimenType;

public class Report {
	private Integer id;
	private String title;
	private List<BusinessIndex> indexList;
	private List<DimenType> dimenList;
	private List<ReportModule> reportModuleList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<BusinessIndex> getIndexList() {
		return indexList;
	}

	public void setIndexList(List<BusinessIndex> indexList) {
		this.indexList = indexList;
	}

	public List<DimenType> getDimenList() {
		return dimenList;
	}

	public void setDimenList(List<DimenType> dimenList) {
		this.dimenList = dimenList;
	}

	public List<ReportModule> getReportModuleList() {
		return reportModuleList;
	}

	public void setReportModuleList(List<ReportModule> reportModuleList) {
		this.reportModuleList = reportModuleList;
	}

}