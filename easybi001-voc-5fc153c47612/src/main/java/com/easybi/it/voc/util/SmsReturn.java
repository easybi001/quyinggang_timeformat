package com.easybi.it.voc.util;

/**
 * 
 * @ClassName: SmsReturn 
 * @Description: 短信信息回复类
 * @author jack_li@cybespoke.com 
 * @date 2015年7月3日 下午2:16:30
 */
public class SmsReturn{
	
	/**
	 * 短信是否发送成功
	 */
	private boolean isSucess = false;
	
	private String code = null;
	
	private String msg = null;
	
	private String smsid = null;

	public boolean isSucess() {
		return isSucess;
	}

	public void setSucess(boolean isSucess) {
		this.isSucess = isSucess;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSmsid() {
		return smsid;
	}

	public void setSmsid(String smsid) {
		this.smsid = smsid;
	}
	
}