package com.easybi.it.voc.util;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @ClassName: SmsUtil 
 * @Description: 短信工具接口
 * @author jack_li@cybespoke.com 
 * @date 2015年7月6日 上午10:44:15
 */

public class SmsUtil {

	private static final Logger logger = LoggerFactory.getLogger(SmsUtil.class);

	private static String URL = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";

	private static String ACCOUNT = "cf_zs3000123";

	private static String PASSWORD = Md5.GetMD5Code("7793495");// 密码可以使用明文密码或使用32位MD5加密

	public static SmsReturn sendSms(String toPhoneNum, String content) {
		SmsReturn sr = new SmsReturn();
		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod(URL);

		// client.getParams().setContentCharset("GBK");
		client.getParams().setContentCharset("UTF-8");
		method.setRequestHeader("ContentType",
				"application/x-www-form-urlencoded;charset=UTF-8");

		NameValuePair[] data = {// 提交短信
		new NameValuePair("account", ACCOUNT),
				new NameValuePair("password", PASSWORD),
				new NameValuePair("mobile", toPhoneNum),
				new NameValuePair("content", content), };
		method.setRequestBody(data);

		try {
			client.executeMethod(method);
			String SubmitResult = method.getResponseBodyAsString();

			// System.out.println(SubmitResult);
			Document doc = DocumentHelper.parseText(SubmitResult);
			Element root = doc.getRootElement();

			String code = root.elementText("code");
			String msg = root.elementText("msg");
			String smsid = root.elementText("smsid");

			logger.debug("code:{},msg:{},smsid:{}", new Object[] { code, msg,
					smsid });

			sr.setCode(code);
			sr.setMsg(msg);
			sr.setSmsid(smsid);
			System.out.println(code);
			if (code.equals("2")) {
				sr.setSucess(true);
			} else {
				sr.setSucess(false);
			}
		} catch (HttpException e) {
			sr.setSucess(false);
			logger.warn("短信发送异常:", e);
		} catch (IOException e) {
			sr.setSucess(false);
			logger.warn("短信发送异常:", e);
		} catch (DocumentException e) {
			sr.setSucess(false);
			logger.warn("短信发送异常:", e);
		}
		return sr;
	}

	public static void main(String[] args) {
		int mobile_code = (int) ((Math.random() * 9 + 1) * 100000);
		// String content = new String("您的验证码是：" + mobile_code +
		// "。请不要把验证码泄露给其他人。");
		// String content = new String("您的验证码是：" + mobile_code +
		// "。请不要把验证码泄露给其他人。");
		String content = "客户姓名：【" + "马昌盛" + "】，手机号码：【" + "13971371586" +
		 "】，详细地址：【"+ "阜华大厦D座12B03" + "】，请尽快与客户联系，谢谢！";
//		String content = "客户订单通知：订单编号【201525465456】，客户姓名：【yangyun】，手机号码：【未填写】，请尽快确认订单。谢谢！";
		SmsReturn sr = sendSms("18221381443", content);
		logger.debug("code:{},msg:{},smsid:{}",
				new Object[] { sr.getCode(), sr.getMsg(), sr.getSmsid() });
	}

}
