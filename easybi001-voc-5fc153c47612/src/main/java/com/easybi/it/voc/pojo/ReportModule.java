package com.easybi.it.voc.pojo;


public class ReportModule {
    private Integer id; // 模块ID
    private ModuleConfig moduleConfig; // 模块配置
    private ModuleCondition moduleCondition; // 模块初始化条件
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ModuleConfig getModuleConfig() {
		return moduleConfig;
	}
	public void setModuleConfig(ModuleConfig moduleConfig) {
		this.moduleConfig = moduleConfig;
	}
	public ModuleCondition getModuleCondition() {
		return moduleCondition;
	}
	public void setModuleCondition(ModuleCondition moduleCondition) {
		this.moduleCondition = moduleCondition;
	}
}