/**
 * @Title: SingleCube.java
 * @Package: com.easybi.it.voc.pojo
 * @Author: jack_li@cybespoke.com
 * @Date: 2016年7月11日
 * @Version: v1.0
 */
package com.easybi.it.voc.pojo;

import java.util.HashMap;

/**
 * @ClassName: SingleCube
 * @Description: 单cube数据类
 */
public class SingleCube extends Cube {
	
	private HashMap<String, Object> originalOptions = new HashMap<String, Object>();
	
	// 查询数据函数
	private Object query() {
		return null;
	}
	
	// 解析cube函数，并生成到类的属性中
	private void parseCubeOptions() {
		
	}
	
	// 解析cube中的dimension，类似于each和all
	private void paseDimensionConf() {
		
	}
	
	// 统一数据查询接口
	private Object queryRawData() {
		return null;
	}
	
	// 把从统一数据接口查出的tree型数据转为matrix型（即多维数组）
	private Object treeToMatrix() {
		return null;
	}
	
	/**
	 * @Title: listToMatrix
	 * @Description: 把从统一数据接口查出的list型数据转为matrix型（即多维数组，暂只支持二维）
	 * @ReturnType: Object
	 */
	private Object listToMatrix() {
		return null;
	}

	/**
	 * @Title: genDimensionInfo
	 * @Description: 获取各个维度的信息
	 * @ReturnType: Object
	 */
	private Object genDimensionInfo() {
		return null;
	}
	
	/**
	 * @Title: genIndexInfo
	 * @Description: TODO
	 * @ReturnType: Object
	 */
	private Object genIndexInfo() {
		return null;
	}
	
	/**
	 * @Title: genDateInfo
	 * @Description: 获取日期信息
	 * @return
	 * @ReturnType: Object
	 */
	private Object genDateInfo() {
		return null;
	}
	
	/**
	 * @Title: genDateRange
	 * @Description: 获取日期范围
	 * @return
	 * @ReturnType: Object
	 */
	private Object genDateRange() {
		return null;
	}
}
