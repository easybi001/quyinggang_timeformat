package com.easybi.it.voc.dao;

import java.util.List;
import java.util.Map;

import com.easybi.it.voc.dto.BusinessIndex;

public interface BusinessIndexMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BusinessIndex record);

    int insertSelective(BusinessIndex record);

    BusinessIndex selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BusinessIndex record);

    int updateByPrimaryKey(BusinessIndex record);
    //获取数据库中所有的指标数据
    List<BusinessIndex> getIndexInfoList(List<String> id);
    
    Map<Integer,BusinessIndex> getIndexInfoMap(List<String> id);
}