package com.easybi.it.voc.api.ctl;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;


public class AbstractViewCtl {
	
	public final Logger logger = LoggerFactory.getLogger(AbstractViewCtl.class);
	
	@ExceptionHandler(Exception.class)
	public String handleException(Exception e, HttpServletResponse response) {
		logger.error("AbstractViewCtl found exception:",e);
	    return "web/505";
	}

}
 