package com.easybi.it.voc.api.ctl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easybi.it.voc.ParamConstants;
import com.easybi.it.voc.api.dto.OpResult;

public class AbstractJsonCtl {
	
	public final Logger logger = LoggerFactory.getLogger(AbstractJsonCtl.class);
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody OpResult handleException(Exception e, HttpServletResponse response, HttpServletRequest request) {
		OpResult r = new OpResult();
		String ipAddress = getRemoteHost(request);
		logger.error("ipAddress:" + ipAddress);
		e.printStackTrace();
		r.setMsg(ParamConstants.E999.getMsg());
		r.setCode(ParamConstants.E999.getCode());
	    return r;
	}
	
	public String getRemoteHost(javax.servlet.http.HttpServletRequest request){
	    String ip = request.getHeader("x-forwarded-for");
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
	        ip = request.getHeader("Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
	        ip = request.getRemoteAddr();
	    }
	    return ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
	}

}
