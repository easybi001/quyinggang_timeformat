package com.easybi.it.voc.dao;

import com.easybi.it.voc.dto.IndexSingleData;

public interface IndexSingleDataMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(IndexSingleData record);

    int insertSelective(IndexSingleData record);

    IndexSingleData selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IndexSingleData record);

    int updateByPrimaryKey(IndexSingleData record);
}