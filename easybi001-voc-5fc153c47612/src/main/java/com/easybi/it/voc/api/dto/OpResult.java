package com.easybi.it.voc.api.dto;

import java.util.Map;

/**
 * 
 * @ClassName: OpResult
 * @Description: 通用的返回函数
 */
public class OpResult extends AbstractDTO {
	private static final long serialVersionUID = 1L;
	private String msg = ""; // 操作的处理信息
	private Map<String, Object> data = null; // 返回的结果类型
	private String code = "";

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the data
	 */
	public Map<String, Object> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
