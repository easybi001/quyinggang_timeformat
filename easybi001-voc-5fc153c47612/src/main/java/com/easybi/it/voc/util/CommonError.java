/**   
 * @Title: CommonError.java 
 * @Package com.easybi.it.util 
 * @author jack_li@cybespoke.com  
 * @date 2016年4月14日 下午4:26:23 
 * @version v1.0   
 */
package com.easybi.it.voc.util;

/** 
 * @ClassName: CommonError 
 * @Description: TODO
 */
public class CommonError {

	private String code;
	private String msg;
	private String detailMsg;
	
	public CommonError(String code, String msg, String detailMsg) {
		this.code = code;
		this.msg = msg;
		this.detailMsg = detailMsg;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	 * @return the detailMsg
	 */
	public String getDetailMsg() {
		return detailMsg;
	}
	/**
	 * @param detailMsg the detailMsg to set
	 */
	public void setDetailMsg(String detailMsg) {
		this.detailMsg = detailMsg;
	}
	
}
