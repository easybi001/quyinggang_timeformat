package com.easybi.it.voc.dao;

import com.easybi.it.voc.dto.DimenType;

public interface DimenTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DimenType record);

    int insertSelective(DimenType record);

    DimenType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DimenType record);

    int updateByPrimaryKey(DimenType record);
}