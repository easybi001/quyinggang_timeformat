package com.easybi.it.voc.dto;

import java.sql.Timestamp;

public class BusinessIndex {
    private Integer id;

    private String name;

    private String shortName;

    private String code;

    private Integer indexClassId;

    private Integer parentId;

    private Integer baseIndex;

    private String indexExplain;

    private Integer indexPrecision;

    private Integer isPercent;

    private String chUnit;

    private String stUnit;

    private Integer indexDecimal;

    private Float trunRate;

    private String remark;

    private Integer status;

    private Timestamp createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getIndexClassId() {
        return indexClassId;
    }

    public void setIndexClassId(Integer indexClassId) {
        this.indexClassId = indexClassId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getBaseIndex() {
        return baseIndex;
    }

    public void setBaseIndex(Integer baseIndex) {
        this.baseIndex = baseIndex;
    }

    public String getIndexExplain() {
        return indexExplain;
    }

    public void setIndexExplain(String indexExplain) {
        this.indexExplain = indexExplain;
    }

    public Integer getIndexPrecision() {
        return indexPrecision;
    }

    public void setIndexPrecision(Integer indexPrecision) {
        this.indexPrecision = indexPrecision;
    }

    public Integer getIsPercent() {
        return isPercent;
    }

    public void setIsPercent(Integer isPercent) {
        this.isPercent = isPercent;
    }

    public String getChUnit() {
        return chUnit;
    }

    public void setChUnit(String chUnit) {
        this.chUnit = chUnit;
    }

    public String getStUnit() {
        return stUnit;
    }

    public void setStUnit(String stUnit) {
        this.stUnit = stUnit;
    }

    public Integer getIndexDecimal() {
        return indexDecimal;
    }

    public void setIndexDecimal(Integer indexDecimal) {
        this.indexDecimal = indexDecimal;
    }

    public Float getTrunRate() {
        return trunRate;
    }

    public void setTrunRate(Float trunRate) {
        this.trunRate = trunRate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}