package com.easybi.it.voc.dao;

import com.easybi.it.voc.dto.DimenValue;

public interface DimenValueMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DimenValue record);

    int insertSelective(DimenValue record);

    DimenValue selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DimenValue record);

    int updateByPrimaryKey(DimenValue record);
}