package com.easybi.it.voc.api.conf;

import java.util.Properties;

/**
 * 
* @ClassName: MyConfig 
* @Description: 系统的配置文件
* @author jack_li@cybespoke.com 
* @date 2015年6月30日 下午5:04:56 
*
 */
public class MyConfig {

	private static Properties prop = new Properties();
	
	public static final String FILE_DIR = "wacker.file.dir";
	
	/**
	 * 重新装置配置
	 * @param newprop
	 */
	public static void load(Properties newprop){
		prop = newprop;
	}
	
	public static Properties getProps(){
		return prop;
	} 
	
	/**
	 * 读取单个系统属性
	 * @param key
	 * @return
	 */
	public static String get(String key){
		return prop.getProperty(key,null);
	}
	
	/**
	 * 设置单个系统属性
	 * @param key
	 * @param value
	 */
	public static void set(String key,String value){
		prop.setProperty(key, value);
	}
	
}
