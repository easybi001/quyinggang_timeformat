package com.easybi.it.voc;


import com.easybi.it.voc.api.conf.MyConfig;
import com.easybi.it.voc.util.CommonError;

/**
 * 
 * @ClassName: ParamConstants 
 * @Description: 系统的常量
 */
public class ParamConstants {
	
	public static final String S_SITE_REQUEST = "S_SITE_REQUEST";

	public static final String S_CUR_SYSADMIN = "S_CUR_SYSADMIN" ;
	
	public static final String LOGIN_USER = "LOGIN_USER" ;
	
	public static final String ORDER_TIMER = "ORDER_TIMER" ;
	
	public static final String IS_ADMIN_ACCESS = "IS_ADMIN_ACCESS";
	
	public static final String PRODUCT_TYPE = "PRODUCT_TYPE";
	
	public static final String PRODUCT_SHOW_TYPE = "PRODUCT_SHOW_TYPE";
	
	public static final String PRODUCT_OTHER_TYPE = "PRODUCT_OTHER_TYPE";
	
	public static final String DESIGN_TYPE = "DESIGN_TYPE";
	
	public static final String NOT_FOUND_PAGE = "web/view/page_not_found";

	public static final String DATA = "data";
	public static final String CODE = "code";
	public static final String MESSAGE = "msg";

	public static final String CLIENT_TYPE_IPAD = "ipad";
	
	// 类别和商品的默认关系
	public static final String[] TYPE_COMMODITY_ARRAY = {"20160414001", "20160414002", "20160414003", "20160414004", 
		"20160414005", "20160414006", "20160414007", "20160414008"};
	
	/**错误代码**/
	public static final CommonError E999 = new CommonError("999", "未知错误", "");
	public static final CommonError E100 = new CommonError("100", "操作成功", "");
	public static final CommonError E2000 = new CommonError("2000", "登录过期", "");
	public static final CommonError E2001 = new CommonError("2001", "用户不存在或手机号码未注册", "");
	public static final CommonError E2002 = new CommonError("2002", "密码错误", "");
	public static final CommonError E2003 = new CommonError("2003", "需要登录的接口必须要有token参数", "");
	public static final CommonError E2004 = new CommonError("2004", "用户尚未登录或token错误", "");
	public static final CommonError E2005 = new CommonError("2005", "短信发送失败", "");
	public static final CommonError E2006 = new CommonError("2006", "验证码错误", "");
	public static final CommonError E2007 = new CommonError("2007", "新密码不合法", "");
	public static final CommonError E2008 = new CommonError("2008", "手机号码已注册", "");
	public static final CommonError E2009 = new CommonError("2009", "图片上传失败", "");
	public static final CommonError E2010 = new CommonError("2010", "地址不存在", "");
	public static final CommonError E2011 = new CommonError("2011", "上传参数不合法", "");
	public static final CommonError E2012 = new CommonError("2012", "顾问等级不够", "");
	public static final CommonError E2013 = new CommonError("2013", "订单不存在", "");
	public static final CommonError E2014 = new CommonError("2014", "商品不存在", "");
	public static final CommonError E2015 = new CommonError("2015", "金额不合法", "");
	public static final String HOST_PATH_PREFIX = "/it";

	public static final String DEFAULT_USER_PASSWORD = "123456";
	
	public static final String UPLOAD_PATH = "ipad";
	
	public static final String HOST_URL = "http://localhost:8080/";
	
	public static final String WEIXIN_PROJECT_NAME = "it-weixin/";
	
	public static final String WEIXIN_PROJECT_FILTER = "app/";
	
	public static final String WEIXIN_PROJECT_PREFIX = HOST_URL + WEIXIN_PROJECT_NAME + WEIXIN_PROJECT_FILTER;
	
	public static final String APP_ID = "wx1e14a4ebececf7ad";
	
	public static final String APP_SECRET = "00ebf944a281fd924f0cd7b0e8736b20";
	
	public static final String TOKEN = "cybespoke";
	
	public static final String CS_PHONENUM = "13671700634";
	
	public static final String WEIXIN_CODE_TYPE = "1";
	
	public static final String WEIXIN_CODE_FROM = "WEIXIN";
	
	public static final String PROMOTION_CODE_TYPE = "2";
	
	public static final String PROMOTION_CODE_FROM = "PROMOTION";
	
	public static final String WEIXIN_MD5_CODE = "2";
	
	public static final String IT_WEB_HOST = "http://www.cybespoke.com/";
}
