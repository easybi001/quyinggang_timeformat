/**
 * @Title: Cube.java
 * @Package: com.easybi.it.voc.pojo
 * @Author: jack@easestrategy.com
 * @Date: 2016年7月11日
 * @Version: v1.0
 */
package com.easybi.it.voc.pojo;

import java.util.HashMap;

/**
 * @ClassName: Cube
 * @Description: 数据单元类
 * 一个Cube就是一个包含数据、条件和结构的对象
 */
public class Cube {
	private int menuId; // 菜单ID
	private int userId; // 用户ID
	private String userIp; // 用户IP
	private String language; // 国际化
	private String mode; // 数据格式
	private HashMap<String, Object> options = new HashMap<String, Object>(); // 参数列表

	// 解析函数
	private void parseOptions() {
		
	}
	
	private HashMap<String, Object> updateOptions(HashMap<String, Object> options) {
		return options;
	}
	
	
}
