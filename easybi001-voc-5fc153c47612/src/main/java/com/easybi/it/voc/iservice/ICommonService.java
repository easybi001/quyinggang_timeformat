/**
 * @Title: ICommonService.java
 * @Package: com.easybi.it.voc.iservice
 * @Author: jack@easestrategy.com
 * @Date: 2016年7月12日
 * @Version: v1.0
 */
package com.easybi.it.voc.iservice;

import com.easybi.it.voc.api.iservice.*;
/**
 * @ClassName: ICommonService
 * @Description: 通用数据读取相关函数
 */
public class ICommonService implements IMyService {

}
