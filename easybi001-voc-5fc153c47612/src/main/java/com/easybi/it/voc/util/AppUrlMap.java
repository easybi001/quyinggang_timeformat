/**   
 * @Title: AppUrlMap.java 
 * @Package com.app.api.util 
 * @Description: TODO 
 * @author jack_li@cybespoke.com  
 * @date 2015年10月31日 下午12:15:56 
 * @version V1.0   
 */
package com.easybi.it.voc.util;

import java.util.ArrayList;

import com.easybi.it.voc.ParamConstants;


/**
 * @ClassName: AppUrlMap
 * @Description: TODO
 * @author jack_li@cybespoke.com
 * @date 2015年10月31日 下午12:15:56
 */
public class AppUrlMap {
	private ArrayList<String> requireLoginUrl = new ArrayList<String>();
	private ArrayList<String> noLoginUrl = new ArrayList<String>();

	// 用户中心下需要登录状态的链接
	private String UPDATE_PASSWORD = ParamConstants.HOST_PATH_PREFIX + "/m/consultant/update-password";
	private String GET_CONSULTANT = ParamConstants.HOST_PATH_PREFIX + "/m/consultant/get-consultant";
	private String LOG_OUT = ParamConstants.HOST_PATH_PREFIX + "/m/consultant/log-out";
	
	// 用户中心下不需要登录状态的链接
	private String LOGIN = ParamConstants.HOST_PATH_PREFIX + "/m/consultant/on-login";

	public AppUrlMap() {
		// 用户中心下需要登录状态
		requireLoginUrl.add(UPDATE_PASSWORD);
		requireLoginUrl.add(GET_CONSULTANT);
		requireLoginUrl.add(LOG_OUT);
		// 用户中心下不需要登录状态
		noLoginUrl.add(LOGIN);

	}

	/**
	 * @return the requireLoginUrl
	 */
	public ArrayList<String> getRequireLoginUrl() {
		return requireLoginUrl;
	}

	/**
	 * @param requireLoginUrl
	 *            the requireLoginUrl to set
	 */
	public void setRequireLoginUrl(ArrayList<String> requireLoginUrl) {
		this.requireLoginUrl = requireLoginUrl;
	}

	/**
	 * @return the noLoginUrl
	 */
	public ArrayList<String> getNoLoginUrl() {
		return noLoginUrl;
	}

	/**
	 * @param noLoginUrl
	 *            the noLoginUrl to set
	 */
	public void setNoLoginUrl(ArrayList<String> noLoginUrl) {
		this.noLoginUrl = noLoginUrl;
	}

}
