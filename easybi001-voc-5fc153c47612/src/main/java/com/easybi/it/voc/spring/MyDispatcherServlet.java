package com.easybi.it.voc.spring;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.easybi.it.voc.api.conf.MyConfig;
import com.easybi.it.voc.api.helper.ServiceHelper;
import com.easybi.it.voc.api.exception.ConfigException;

/**
 * 
 * @ClassName: MyDispatcherServlet
 * @Description: 系统配置相关的入口
 */
public class MyDispatcherServlet extends DispatcherServlet {

	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory
			.getLogger(MyDispatcherServlet.class); // 日志
	private static AbstractApplicationContext springctx = null; // spring配置相关

	/**
	 * 
	 * @param config
	 *            配置参数
	 * @throws ServletException
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		logger.info("sysetm init");
		logger.info("sysetm load config start.");
		String osName = System.getProperty("os.name");
		String filePath = "apparel/conf/it.conf";		// 配置文件路径
		if (osName.equals("Windows 7")) { // 根据服务器环境不同，配置文件在不同的目录下
			filePath = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\bin\\it.conf";
		} else if (osName.equals("Windows Server 2008 R2")) {
			filePath = "C:\\Program Files\\tomcat\\apache-tomcat-7.0.64\\bin\\web\\conf\\it.conf";
		}
		File sysconfig = new File(filePath);
		logger.info("sysetm load config from {}", sysconfig.getAbsolutePath());
		Properties prop = new Properties();
		InputStreamReader is = null;
		try {
			is = new InputStreamReader(new FileInputStream(sysconfig), "UTF-8");
			prop.load(is);
			MyConfig.load(prop);
		} catch (FileNotFoundException e) {
			throw new ConfigException("it.conf not found.", e);
		} catch (IOException e) {
			throw new ConfigException("it.conf read fail.", e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					logger.warn("it.conf InputStreamReader close error.");
				}
			}
		}
		logger.info("sysetm load config ok.");
		logger.info("sysetm load service spring start.");
		springctx = new ClassPathXmlApplicationContext(
				new String[] { "classpath*:spring-app-local.xml" });
		ServiceHelper.setApplicationContext(springctx);
		logger.info("sysetm load service spring ok.");
		logger.info("sysetm init ok.");
		super.init(config);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
	}

	@Override
	public void destroy() {
		springctx.close();
		super.destroy();
	}
}
