package com.easybi.it.voc.pojo;

import java.util.List;

import com.easybi.it.voc.dto.DimenType;
import com.easybi.it.voc.dto.DimenValue;

public class ModuleDimen {
    private DimenType dimenType;
    private List<DimenValue> dimenValueList;
    
    public DimenType getDimenType() {
		return dimenType;
	}

	public void setDimenType(DimenType dimenType) {
		this.dimenType = dimenType;
	}

	public List<DimenValue> getDimenValueList() {
		return dimenValueList;
	}

	public void setDimenValueList(List<DimenValue> dimenValueList) {
		this.dimenValueList = dimenValueList;
	}

	public DimenValue getFirstDimenValue() {
    	if (dimenValueList.size() > 0) {
    		return dimenValueList.get(0);
    	} else {
    		return null;
    	}
    }
    
    public DimenValue getLastDimenValue() {
    	if (dimenValueList.size() > 0) {
    		return dimenValueList.get(dimenValueList.size() - 1);
    	} else {
    		return null;
    	}
    }
}