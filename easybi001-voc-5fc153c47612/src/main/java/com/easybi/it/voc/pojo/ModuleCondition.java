package com.easybi.it.voc.pojo;

import java.util.List;

import com.easybi.it.voc.dto.BusinessIndex;

public class ModuleCondition {
	private List<BusinessIndex> selectIndexList; // 选中的指标
	private String startDate; // 开始时间
	private String endDate; // 结束时间
	private String chartsType; // 选择的图形类型
	private String dimenTypeSelected; // 选择的维度类型
	private String dimenValueSelected;// 选择的值类型
	public List<BusinessIndex> getSelectIndexList() {
		return selectIndexList;
	}
	public void setSelectIndexList(List<BusinessIndex> selectIndexList) {
		this.selectIndexList = selectIndexList;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getChartsType() {
		return chartsType;
	}
	public void setChartsType(String chartsType) {
		this.chartsType = chartsType;
	}
	public String getDimenTypeSelected() {
		return dimenTypeSelected;
	}
	public void setDimenTypeSelected(String dimenTypeSelected) {
		this.dimenTypeSelected = dimenTypeSelected;
	}
	public String getDimenValueSelected() {
		return dimenValueSelected;
	}
	public void setDimenValueSelected(String dimenValueSelected) {
		this.dimenValueSelected = dimenValueSelected;
	}
	
}